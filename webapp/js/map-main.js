var map, popup;

var HOST = "http://ewis.pld.ttu.ee";

/*geojson style and highlighting functions*/
function style(feature) {
    return {
        weight: 2,
        opacity: 0.9,
        color: '#0083CF',
        fillOpacity: 0.2
    };
}
function highlightFeature(e) {
    var layer = e.target;
    layer.setStyle({
        weight: 2,
        opacity: 0.9,
        color: '#F00',
        fillOpacity: 0.2
    });
}
function resetHighlight(e) {
    var layer = e.target
    layer.setStyle({
        weight: 2,
        opacity: 0.9,
        color: '#0083CF',
        fillOpacity: 0.2
    });
}
function zoomToFeature(e) {
    map.fitBounds(e.target.getBounds());
}
function onEachFeature(feature, layer) {
    layer.on({
        mouseover: highlightFeature,
        mouseout: resetHighlight,
        click: zoomToFeature
    });
}
function getFeature(layer, feature) {

    var geoJsonUrl = HOST+"/geoserver/ewis/ows" +
        "?service=WFS" +
        "&version=1.0.0" +
        "&request=GetFeature" +
        "&typeName=" + layer +
        "&maxFeatures=1" +
        "&outputFormat=text/javascript" +
        "&srsName=EPSG:4326" +
        "&format_options=callback:loadGeoJson" +
        "&featureID="+feature;
    console.log(geoJsonUrl);
    $.ajax({
        jsonpCallback: 'loadGeoJson',
        url: geoJsonUrl,
        dataType: 'jsonp',
        success: function (data) {
            console.log("success");
            geojson.clearLayers();
            geojson.addData(data);
        },
        error: function () {
            console.log("Error loading geojson");
        }
    });
}

var crs = L.CRS.proj4js('EPSG:3301', '+proj=lcc +lat_1=59.33333333333334 +lat_2=58 +lat_0=57.51755393055556 ' +
    '+lon_0=24 +x_0=500000 +y_0=6375000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs',
    new L.Transformation(1, -40500, -1, 7017000));

crs.scale = function(zoom) {
    return 1 / (4000 / Math.pow(2, zoom));
};

// wms template
//var heitvesi = new L.TileLayer.WMS(HOST+"/geoserver/ewis/wms", {
//    layers: 'ewis:heitvesi',
//    format: 'image/png',
//    transparent: true
//});

//gwc wms
//var vooluvesi = new L.TileLayer.WMS(HOST+"/geoserver/ewis/wms", {
//    layers: 'ewis:vooluvesi',
//    format: 'image/png',
//    transparent: true
//});


var jarv = new L.TileLayer(HOST+"/geoserver/gwc/service/tms/1.0.0/ewis%3Ajarv@EPSG%3A3301@png/{z}/{x}/{y}.png",
    {tms: true,
    noWrap: true,
    reuseTiles: true,
    updateWhenIdle: true,
    tileSize: 256
});

var corine = new L.TileLayer(HOST+"/geoserver/gwc/service/tms/1.0.0/ewis%3Acorine2006@EPSG%3A3301@png/{z}/{x}/{y}.png",
    {tms: true,
    noWrap: true,
    reuseTiles: true,
    updateWhenIdle: true,
    tileSize: 256
});

var vooluvesi = new L.TileLayer(HOST+"/geoserver/gwc/service/tms/1.0.0/ewis%3Avooluvesi@EPSG%3A3301@png/{z}/{x}/{y}.png",
    {tms: true,
    noWrap: true,
    reuseTiles: true,
    updateWhenIdle: true,
    tileSize: 256
});

var heitvesi = new L.TileLayer(HOST+"/geoserver/gwc/service/tms/1.0.0/ewis%3Aheitvesi@EPSG%3A3301@png/{z}/{x}/{y}.png",
    {tms: true,
    noWrap: true,
    reuseTiles: true,
    updateWhenIdle: true,
    tileSize: 256
});

var subbasins = new L.TileLayer(HOST+"/geoserver/gwc/service/tms/1.0.0/ewis%3Aalamvalgalad@EPSG%3A3301@png/{z}/{x}/{y}.png",
    {tms: true,
    noWrap: true,
    reuseTiles: true,
    updateWhenIdle: true,
    tileSize: 256
});

var basins = new L.TileLayer(HOST+"/geoserver/gwc/service/tms/1.0.0/ewis%3Avalgalad@EPSG%3A3301@png/{z}/{x}/{y}.png",
    {tms: true,
    noWrap: true,
    reuseTiles: true,
    updateWhenIdle: true,
    tileSize: 256
});

// bounds: http://tiles.maaamet.ee/tm/s/1.0.0/kaart/
var maaamet = new L.TileLayer("http://tiles.maaamet.ee/tm/s/1.0.0/kaart/{z}/{x}/{y}.png",
    {tms: true,
    reuseTiles: true,
    updateWhenIdle: true,
    attribution: "Aluskaart: <a href='http://www.maaamet.ee'>Maa-amet</a>",
    tileSize: 256
});

var osm = new L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png');

var baseLayers = {
    "Aluskaart": maaamet
};

var overlays = {
/*For enabling corine and alamvalgalad, geoserver must be configured*/
    //"Corine": corine,
    "Valgalad": basins,
    //"Alamvalgalad": subbasins,
    "Heitvesi": heitvesi,
    "Järved": jarv,
    "Jõed": vooluvesi
};

map = new L.Map('map', {
    crs: crs,
    center: new L.LatLng(58.5, 25.4),
    zoom: 3,
    maxZoom: 13,
    minZoom: 3,
    maxBounds: new L.LatLngBounds(new L.LatLng(56,17), new L.LatLng(60, 35)),
    layers: [maaamet],
    zoomControl: true
});

var geojson = L.geoJson(null, {
    style: style,
    onEachFeature: onEachFeature}).addTo(map);


//getFeature("ewis:vooluvesi", "1890");

/*Controls*/
L.control.layers(baseLayers, overlays, {collapsed: false}).addTo(map);
map.addEventListener('click', onMapClick);

popup = new L.Popup({
    maxWidth: 400
});

function onMapClick(e) {
	var latlngStr = '(' + e.latlng.lat.toFixed(3) + ', ' + e.latlng.lng.toFixed(3) + ')';
	var bbox = map.getBounds()._southWest.lng+","+map.getBounds()._southWest.lat+","+map.getBounds()._northEast.lng+","+map.getBounds()._northEast.lat;
	var width = map.getSize().x;
	var height = map.getSize().y;
	var x = map.layerPointToContainerPoint(e.layerPoint).x;
	var y = map.layerPointToContainerPoint(e.layerPoint).y;

	popup.setLatLng(e.latlng);

//    popup.setContent("Coords: "+e.latlng +"\n Zoom:"+map.getZoom());
//    map.openPopup(popup);

	/*Check which tool is enabled*/
	var toolList = ["bt_data_tool"];
	for (var i=0; i < toolList.length; i++){
		tool = toolList[i];
		var classList = document.getElementById(tool).className.split(/\s+/);
		for (var j = 0; j < classList.length; j++) {
			if (classList[j] != "selected") continue;
		    switch(tool){
		    	case "bt_data_tool":
		    		var layername = "ewis:vooluvesi";
		    		getRiverData(bbox, width, height, x, y, layername);
		    	break;
                default:

                break;
		    }
		}
	}
}

var riverFeatureID;
function getRiverData(bbox, width, height, x, y, layername){
    var resturl = "rest/riverdd/layername="+layername+
        "&bbox="+bbox+
        "&height="+height+
        "&width="+width+
        "&x="+x+
        "&y="+y;

    $.ajax({
        type: "GET",
        url: resturl,
        contentType:"application/json; charset=utf-8",
        dataType:"json",
        success: function(result) {
            $("#code").val(result.code);
            $("#name").val(result.name);
            $("#length").val(result.length);
            $("#width").val(result.width);
            $("#m_index").val(result.m_index);
            riverFeatureID=result.code;
        }
    });
}

function getFeatureInfo(bbox, width, height, x, y, layername){
	var resturl = "rest/getfeatureinfo/layername="+layername+
                  		"&bbox="+bbox+
                  		"&height="+height+
                  		"&width="+width+
                  		"&x="+x+
                  		"&y="+y;

	$.ajax({
		type: "GET",
		url: resturl,
		contentType:"application/json; charset=utf-8",
        dataType:"json",
		success: function(result) {
			popup.setContent(result[0].nimi);
			map.openPopup(popup);
		},
		error: function(x,st,er) {
				popup.setContent(x + "\n" + st + "\n" +  er + "\n" +resturl);
				map.openPopup(popup);
		}
	});
}