/*Wennerblomi sisestusv2ljad*/
var inputFields = ["B2","B3","B4","B5","B7","B8","B9","B10","B11","B12","B13","B14","B15","B17","B18","B34","B35",
    "B36","B37","B38","B39","B40","B41","B42","B43","B44","B47","B48","B49","B50","B51","B52","B53","B54","B55",
    "B56","B57","B58","B59","B60","B61","B62","B63","B64","B65","B66","B69","B70","B71","B68","B72"];

/*(näidis)andmete pärimine*/
function getWbData(data) {
    $.ajax({
        type: "GET",
        url: "rest/wennerblom/sample/" + data,
        dataType: "json",
        success: function(result){
            for(var i = 0; i < inputFields.length; i += 1){
                $("#"+inputFields[i]).val(result[i][inputFields[i]]);
            }
        }
    });
}

/*initialize buttons, windows etc*/
function initElements() {
    for(var i = 0; i < inputFields.length; i += 1){
        $("#"+inputFields[i]).val("");
    }

    /*alguses peidetavad aknad*/
    $("#window-riverdd").hide();
    $("#window-wennerblom").hide();
    $("#window-rivers").hide();

    /*Lohistatavad aknad*/
    $("#window-map").draggable({ snap: true, handle: ".handler", containment: "#desktop", scroll: false});
    $("#window-riverdd").draggable({ snap: true, handle: ".handler", containment: "#desktop", scroll: false});
    $("#window-wennerblom").draggable({ snap: true, handle: ".handler", containment: "#desktop", scroll: false});

    /*resizable windows*/
    $("#window-wennerblom").resizable({alsoResize: "#window-wennerblom #wb-table",
        maxWidth: 500,
        minWidth: 500});

    $("#window-map").resizable({alsoResize: "#map"});
    $("#map").resizable();

    /*Nupu alla vajutades stiili muutmine*/
    $(".module-button").click(function () {
        $(this).toggleClass("selected");
    });

    /*Moodulite nupud*/
    $("#bt_module_map").click(function () {   //kaardiaken
        $("#window-map").toggle();
    });
    $("#bt_module_1").click(function () {     //j6eproov
        $("#window-riverdd").toggle();
    });

    $("#bt_module_2").click(function () {     //wennerblom
        $("#window-wennerblom").toggle();
        $(".wb-results").toggle();

    });

    $("#bt_rivers").click(function () {      //j6gede valik
        $("#window-rivers").toggle();

        /*force tiling*/
        if ($("#window-rivers").is(":visible")) {
            $("#window-map").css("width", "85%");
            $("#map").css("width", "100%");
        } else {
            $("#window-map").css("width", "100%");
            $("#map").css("width", "100%");
        }
    });

    /*Aktiivse akna esiletoomine*/
    $(".handler").mousedown(function () {
        $(".window").removeClass("front");
        $(this).parent().addClass("front");
    });

    $(".module-button").click(function(){
        var a=0;
        $(".window").each(function(){
            if($(this).is(":visible")){
                a+=1;
            }
        });
        if(a==0){
            $("#empty").fadeIn(500);
        } else {
            $("#empty").fadeOut(500);
        }
    });
}

/*------J6eproovi moodul------*/
function riverdd() {
    $("#bt_data_tool").click(function () {
        $(this).toggleClass("selected");
        if ($("#map").css('cursor') == 'crosshair')$("#map").css('cursor', 'default');
        else $("#map").css('cursor', 'crosshair');
    });

    $("#bt_submit").click(function () {
        var resturl = "rest/riverdd/calculate/" +
            "inflow=" + $("#window-riverdd input:checked").val() +
            "&mi=" + $("#m_index").val() +
            "&width=" + $("#width").val() +
            "&depth=" + $("#depth").val();
        $.ajax({
            type: "GET",
            url: resturl,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (result) {
                $("#riverdd-result").text(result.distance + " m")
                getFeature("ewis:vooluvesi_shp", "vooluvesi.23");
            }
        });
    });
}


/*Main function*/
$(function(){
    initElements();

    riverdd();

    /*------Wennerblomi moodul------*/

    /*arvuta nupp*/
    res_counter=0; // tulemuste loendur
    $("#bt_wb_calc").click(function(){

        resturl =  'rest/wennerblom/calc/';
        for(var i = 0; i < inputFields.length; i += 1){
            resturl+= $.trim($("#"+inputFields[i]).val()) + "&";
        }

        $.ajax({
            type: "GET",
            url: resturl,
            contentType:"application/json; charset=utf-8",
            dataType:"json",
            success: function(result) {

                res_counter++;
                var title = $("#B2").val();
                var id = "window-wb-results-"+res_counter;

                /*javascriptiga tulemuste akna kirjutamine htmli... Ilusam lahendus oleks tore.*/
                $("#desktop").append("" +
                    "<div id='"+id+"' class='window wb-results'>\
                <div class='handler'>"+title+"<div class='bt_close'></div></div>\
                <div class='window-wrapper'>\
                <ul>\
                    <li><a href='#tabs-1'>Tabel 1</a></li>\
                    <li><a href='#tabs-21'>Tabel 2-1</a></li>\
                    <li><a href='#tabs-22'>Tabel 2-2</a></li>\
                    <li><a href='#tabs-3'>Graafik N</a></li>\
                    <li><a href='#tabs-4'>Graafik P</a></li>\
                </ul>\
                <div id='tabs-1'><div class='wb-table'></div></div>\
                <div id='tabs-21'><div class='wb-table'></div></div>\
                <div id='tabs-22'><div class='wb-table'></div></div>\
                <div id='tabs-3'><div class='graph'></div></div>\
                <div id='tabs-4'><div class='graph'></div></div>\
                </div>\
                </div>\
                ");
                /*jquery tabs*/
                $( "#"+id+" .window-wrapper" ).tabs();

                /*akende funktsionaalsuse laiendamine allesloodud aknale.*/
                $( "#"+id ).draggable({ snap: true, handle: ".handler", containment: "#desktop", scroll: false});

                /*aktiivse akna esiletoomine.*/
                $(".handler").mousedown(function(){
                    $(".window").removeClass("front");
                    $(this).parent().addClass("front");
                });

                /*Sulgemise nupp*/
                $(".bt_close").click(function(){
                    $(this).parent().parent().remove();
                });

                /*tabel1*/
                $( "#"+id+" #tabs-1 .wb-table").html("<table>" +
                    "<tr><td>&nbsp;</td><td colspan='2'>Lämmastik</td><td colspan='2'>Fosfor</td></tr>" +
                    "<tr><td>&nbsp;</td><td>t/a</td><td>%</td><td>kg/a</td><td>%</td></tr>" +
                    "<tr><td>Ärakanne põllumaalt</td><td>"+result.N9+"</td>" +
                        "<td>"+result.N9p+"</td><td>"+result.P9+"</td><td>"+result.P9p+"</td></tr>"+
                    "<tr><td>Ärakanne metsadest</td><td>"+result.N10+"</td><td>"+result.N10p+"</td>" +
                        "<td>"+result.P10+"</td><td>"+result.P10p+"</td></tr>"+
                    "<tr><td>Metsamajandus</td><td>"+result.N11+"</td><td>"+result.N11p+"</td>" +
                        "<td>"+result.P11+"</td><td>"+result.P11p+"</td></tr>"+
                    "<tr><td>Ärakanne märgaladelt</td><td>"+result.N12+"</td><td>"+result.N12p+"</td>" +
                        "<td>"+result.P12+"</td><td>"+result.P12p+"</td></tr>"+
                    "<tr><td>Ärakanne muudelt maadelt</td><td>"+result.N13+"</td><td>"+result.N13p+"</td>" +
                        "<td>"+result.P13+"</td><td>"+result.P13p+"</td></tr>"+
                    "<tr><td>Sadenemine järvede pinnale</td><td>"+result.N14+"</td><td>"+result.N14p+"</td>" +
                        "<td>"+result.P14+"</td><td>"+result.P14p+"</td></tr>"+
                    "<tr><td>RP (s.h. otsevool ja sademevesi)</td><td>"+result.N15+"</td><td>"+result.N15p+"</td>" +
                        "<td>"+result.P15+"</td><td>"+result.P15p+"</td></tr>"+
                    "<tr><td>Omapuhastid</td><td>"+result.N16+"</td><td>"+result.N16p+"</td>" +
                        "<td>"+result.P16+"</td><td>"+result.P16p+"</td></tr>"+
                    "<tr><td>Lüpsikojad</td><td>"+result.N17+"</td><td>"+result.N17p+"</td>" +
                        "<td>"+result.P17+"</td><td>"+result.P17p+"</td></tr>"+
                    "<tr><td>Sõnnikuhoidlad</td><td>"+result.N18+"</td><td>"+result.N18p+"</td>" +
                        "<td>"+result.P18+"</td><td>"+result.P18p+"</td></tr>"+
                    "<tr><td>Kalakasvatus</td><td>"+result.N19+"</td><td>"+result.N19p+"</td>" +
                        "<td>"+result.P19+"</td><td>"+result.P19p+"</td></tr>"+
                    "<tr><td>Tööstuse otseheide suublasse</td><td>"+result.N20+"</td><td>"+result.N20p+"</td>" +
                        "<td>"+result.P20+"</td><td>"+result.P20p+"</td></tr>"+
                    "<tr><td><b>KOKKU</b></td><td>"+result.N21+"</td><td>"+result.N21p+"</td>" +
                    "<td>"+result.P21+"</td><td>"+result.P21p+"</td></tr>"+
                    "</table>");

                /*tabel2-1*/
                $( "#"+id+" #tabs-21 .wb-table").html("<table>" +
                    "<tr><td>&nbsp;</td><td colspan='2'>Lämmastik</td><td colspan='2'>Fosfor</td></tr>" +
                    "<tr><td>&nbsp;</td><td>t/a</td><td>%</td><td>kg/a</td><td>%</td></tr>" +
                    "<tr><td colspan='5'>LOODUSLIK ÄRAKANNE</td></tr>"+
                    "<tr><td>Metsad</td><td>"+result.N30+"</td><td>"+result.N30p+"</td>" +
                        "<td>"+result.P30+"</td><td>"+result.P30p+"</td></tr>"+
                    "<tr><td>Põllumaa</td><td>"+result.N31+"</td><td>"+result.N31p+"</td>" +
                        "<td>"+result.P31+"</td><td>"+result.P31p+"</td></tr>"+
                    "<tr><td>Märgalad</td><td>"+result.N32+"</td><td>"+result.N32p+"</td>" +
                        "<td>"+result.P32+"</td><td>"+result.P32p+"</td></tr>"+
                    "<tr><td>Muu maa</td><td>"+result.N33+"</td><td>"+result.N33p+"</td>" +
                        "<td>"+result.P33+"</td><td>"+result.P33p+"</td></tr>"+
                    "<tr><td>Sadenemine järvede pinnale</td><td>"+result.N34+"</td><td>"+result.N34p+"</td>" +
                        "<td>"+result.P34+"</td><td>"+result.P34p+"</td></tr>"+
                    "<tr><td><b>LOODUSLIK ÄRAKANNE KOKKU</b></td><td>"+result.N36+"</td><td>100</td>" +
                        "<td>"+result.P36+"</td><td>100</td></tr>"+
                    "<tr><td>osa koguärakandest (%)</td><td>&nbsp;</td><td>"+result.N36p+"</td><td>&nbsp;</td><td>"+result.P36p+"</td></tr>"+
                    "</table>");

                /*tabel2-2*/
                $( "#"+id+" #tabs-22 .wb-table").html("<table>" +
                        "<tr><td>&nbsp;</td><td colspan='2'>Lämmastik</td><td colspan='2'>Fosfor</td></tr>" +
                        "<tr><td>&nbsp;</td><td>t/a</td><td>%</td><td>kg/a</td><td>%</td></tr>" +
                        "<tr><td colspan='5'>INIMLÄHTENE ÄRAKANNE</td></tr>"+
                    "<tr><td>Järved - sadenemine õhust</td><td>"+result.N40+"</td><td>"+result.N40p+"</td>" +
                        "<td>"+result.P40+"</td><td>"+result.P40p+"</td></tr>"+
                    "<tr><td>Metsad tänu õhusaastele</td><td>"+result.N41+"</td><td>"+result.N41p+"</td>" +
                        "<td>"+result.P41+"</td><td>"+result.P41p+"</td></tr>"+
                    "<tr><td>Põllumaa tänu õhusaastele</td><td>"+result.N42+"</td><td>"+result.N42p+"</td>" +
                        "<td>"+result.P42+"</td><td>"+result.P42p+"</td></tr>"+
                    "<tr><td>Metsandus - lageraie</td><td>"+result.N43+"</td><td>"+result.N43p+"</td>" +
                        "<td>"+result.P43+"</td><td>"+result.P43p+"</td></tr>"+
                    "<tr><td>Metsandus - kuivendamine</td><td>"+result.N44+"</td><td>"+result.N44p+"</td>" +
                        "<td>"+result.P44+"</td><td>"+result.P44p+"</td></tr>"+
                    "<tr><td>Metsandus - väetamine</td><td>"+result.N45+"</td><td>"+result.N45p+"</td>" +
                        "<td>"+result.P45+"</td><td>"+result.P45p+"</td></tr>"+
                    "<tr><td>Põllumajandus</td><td>"+result.N46+"</td><td>"+result.N46p+"</td>" +
                        "<td>"+result.P46+"</td><td>"+result.P46p+"</td></tr>"+
                    "<tr><td>Lüpsikojad</td><td>"+result.N47+"</td><td>"+result.N47p+"</td>" +
                        "<td>"+result.P47+"</td><td>"+result.P47p+"</td></tr>"+
                    "<tr><td>Sõnnikuhoidlad</td><td>"+result.N48+"</td><td>"+result.N48p+"</td>" +
                        "<td>"+result.P48+"</td><td>"+result.P48p+"</td></tr>"+
                    "<tr><td>Omapuhastid</td><td>"+result.N49+"</td><td>"+result.N49p+"</td>" +
                        "<td>"+result.P49+"</td><td>"+result.P49p+"</td></tr>"+
                    "<tr><td>Reoveepuhastid (v.a. otsevool)</td><td>"+result.N50+"</td><td>"+result.N50p+"</td>" +
                        "<td>"+result.P50+"</td><td>"+result.P50p+"</td></tr>"+
                    "<tr><td>Otsevoolu- ja sademevesi</td><td>"+result.N51+"</td><td>"+result.N51p+"</td>" +
                        "<td>"+result.P51+"</td><td>"+result.P51p+"</td></tr>"+
                    "<tr><td>Tööstuse otseheide suublasse</td><td>"+result.N52+"</td><td>"+result.N52p+"</td>" +
                        "<td>"+result.P52+"</td><td>"+result.P52p+"</td></tr>"+
                    "<tr><td>Kalakasvatus</td><td>"+result.N53+"</td><td>"+result.N53p+"</td>" +
                        "<td>"+result.P53+"</td><td>"+result.P53p+"</td></tr>"+
                    "<tr><td><b>INIMLÄHTENE ÄRAKANNE KOKKU</b></td><td>"+result.N55+"</td><td>100</td>" +
                    "<td>"+result.P55+"</td><td>100</td></tr>"+
                    "<tr><td>osa koguärakandest (%)</td><td>&nbsp;</td><td>"+result.N55p+"</td><td>&nbsp;</td><td>"+result.P55p+"</td></tr>"+
                    "</table>");

                //l2mmastiku graafik
                $("#"+id+" #tabs-3 .graph").each(function(){
                    new Highcharts.Chart({
                        chart: {
                            renderTo: this,
                            type: 'bar'
                        },
                        title: {
                            text: 'Lämmastiku ärakanne'
                        },
                        subtitle: {
                            text: 'Nitrogen runoff'
                        },
                        xAxis: {
                            categories: ['Sadenemine järvede pinnale',
                                'Ärakanne metsadest',
                                'Ärakanne põllumaadelt',
                                'Ärakanne märgaladelt',
                                'Muu',
                                'Septic tanks',
                                'Wastewater treatment',
                                'Runoff and rain',
                                'Industrial waste water',
                                'Fish farming'],
                            title: {
                                text: "Lämmastik"
                            }
                        },
                        yAxis: {
                            min: 0,
                            title: {
                                text: 'tonni aastas',
                                align: 'high'
                            },
                            labels: {
                                overflow: 'justify'
                            }
                        },
                        tooltip: {
                            formatter: function () {
                                return '' +
                                    this.series.name + ': ' + this.y + ' tons';
                            }
                        },
                        plotOptions: {
                            bar: {
                                dataLabels: {
                                    enabled: true
                                }
                            }
                        },
                        credits: {
                            enabled: false
                        },
                        series: [
                            {
                                name: 'Looduslik',
                                data: [result.N34, result.N30, result.N31, result.N32, result.N33]
                            },
                            {
                                name: 'Ärakanne',
                                data: [result.N14, result.N10, result.N9, result.N12, result.N13]
                            }
//                            ,
//                            {
//                                name: 'Air pollution',
//                                data: [result.N11, result.N11, result.N11, result.N11, result.N11]
//                            }
                        ]
                    });

                });

                //Fosfori graafik
                $("#"+id+" #tabs-4 .graph").each(function(){
                    new Highcharts.Chart({
                        chart: {
                            renderTo: this,
                            type: 'bar'
                        },
                        title: {
                            text: 'Fosfori ärakanne'
                        },
                        subtitle: {
                            text: 'Phosphorus runoff'
                        },
                        xAxis: {
                            categories: ['Sadenemine järvede pinnale',
                                'Ärakanne metsadest',
                                'Ärakanne põllumaadelt',
                                'Ärakanne märgaladelt',
                                'Muu',
                                'Septic tanks',
                                'Wastewater treatment',
                                'Runoff and rain',
                                'Industrial waste water',
                                'Fish farming'],
                            title: {
                                text: "Fosfor"
                            }
                        },
                        yAxis: {
                            min: 0,
                            title: {
                                text: 'tonni aastas',
                                align: 'high'
                            },
                            labels: {
                                overflow: 'justify'
                            }
                        },
                        tooltip: {
                            formatter: function () {
                                return '' +
                                    this.series.name + ': ' + this.y + ' tons';
                            }
                        },
                        plotOptions: {
                            bar: {
                                dataLabels: {
                                    enabled: true
                                }
                            }
                        },
                        credits: {
                            enabled: false
                        },
                        series: [
                            {
                                name: 'Looduslik',
                                data: [result.P34, result.P30, result.P31, result.P32, result.P33]
                            },
                            {
                                name: 'Ärakanne',
                                data: [result.P14, result.P10, result.P9, result.P12, result.P13]
                            }
//                            ,
//                            {
//                                name: 'Air pollution',
//                                data: [result.P9, result.P9, result.P9, result.P9, result.P9, result.P9]
//                            }
                        ]
                    });

                });


            },
            error: function() {
                alert("ERROR!");
            }
        });
	});


	/*J6gede hierarhia puu*/
	$("#tree").dynatree({
		onActivate: function(node) {
            getWbData(node.data.title);

            if (node.data.title == "keila1"){
                getFeature("ewis:valgalad", "323");
            }
            if (node.data.title == "keila2"){
                getFeature("ewis:valgalad", "322");
            }
            if (node.data.title == "keila3"){
                getFeature("ewis:valgalad", "321");
            }
		},
		children: [
			{title: "Näidisandmed", isFolder: true, key: "examples",
				children: [
					{title: "keila1"},
					{title: "keila2"},
					{title: "keila3"}
				]
			}
		]
	});

});
