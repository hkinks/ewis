package ee.era.ewis.rest;

import org.junit.Test;

import java.sql.ResultSet;

import static org.junit.Assert.assertEquals;

public class PSQLConnectionTest {
    @Test
    public void testConnection() throws Exception {
        PSQLConnection connection = new PSQLConnection();
        ResultSet resultSet= connection.getResultSet("select version();");
        resultSet.next();
        String result = resultSet.getString(1);
        assertEquals(result.substring(0, 10), "PostgreSQL");
    }
}
