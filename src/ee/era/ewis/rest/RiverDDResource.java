package ee.era.ewis.rest;

import ee.era.ewis.EwisLauncher;
import ee.era.ewis.model.River;
import ee.era.ewis.model.RiverDDModel;
import org.apache.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONStringer;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import java.io.IOException;
import java.net.URL;
import java.sql.ResultSet;
import java.sql.SQLException;

@Path("riverdd")
@Produces("application/json")
public class RiverDDResource {

    static Logger logger = Logger.getLogger(RiverDDResource.class.getName());

    @GET
    @Path("layername={layername}" +
            "&bbox={bbox}" +
            "&height={height}" +
            "&width={width}" +
            "&x={x}" +
            "&y={y}")
    public String getData(@PathParam("layername") String layername,
                          @PathParam("bbox") String bbox,
                          @PathParam("height") int height,
                          @PathParam("width") int width,
                          @PathParam("x") int x,
                          @PathParam("y") int y) throws IOException, JSONException {
        /*geoserveri wms-i urli moodustamine saadud parameetritest*/
        URL url = new URL("http://"+ EwisLauncher.HOST +"/geoserver/ewis/wms?service=WMS"+
                "&version=1.1.0" +
                "&request=GetFeatureInfo" +
                "&layers=" + layername +
                "&query_layers=" + layername +
                "&styles=&bbox=" + bbox +
                "&feature_count=1" +
                "&height=" + height +
                "&width=" + width +
                "&format=image%2fpng" +
                "&info_format=text%2fhtml" +
                "&srs=epsg%3a4326" +
                "&x=" + x +
                "&y=" + y);


        /*urli p2ring ja jsoni lugemine*/
        URLReader urlReader = new URLReader(url);
        JSONObject jsonObject = urlReader.getJSONOuptut();
        //System.out.println(urlReader.getOutput());
        logger.debug(url);
        /*kui ei 6nnestu lugeda ja jsoni objekti ei tagastata*/
        if (jsonObject == null) return null;

        River river = new River();
        river.setName((String) jsonObject.get("nimi"));
        String idString = jsonObject.get("kr_kood").toString().substring(3);
        river.setId(Long.valueOf(idString));

        PSQLConnection connection = new PSQLConnection();
        String query = "SELECT pikkus_km,pindala_km2,algus_x,algus_y,lopp_x,lopp_y FROM joe_andmed WHERE id_jogi='" + idString + "' LIMIT 1";
        ResultSet resultSet = connection.getResultSet(query);

        try {
            resultSet.next();
            river.setLength(resultSet.getFloat(1));
            String areaString = resultSet.getString(2);
            if(!areaString.isEmpty())river.setArea(Float.valueOf(resultSet.getString(2)));
            river.setDepth(1.0f);
            river.setBeginningPoint(resultSet.getFloat(3), resultSet.getFloat(4));
            river.setEndingPoint(resultSet.getFloat(5), resultSet.getFloat(6));
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return new JSONStringer()
                .object()
                    .key("code")
                    .value(river.getId())
                    .key("name")
                    .value(river.getName())
                    .key("length")
                    .value(river.getLength())
                    .key("m_index")
                    .value(river.getMeanderIndex())
                    .key("width")
                    .value(river.getWidth())
                .endObject()
                .toString();
    }

    @GET
    @Path("calculate/" +
            "inflow={inflow}" +
            "&mi={mi}" +
            "&width={width}" +
            "&depth={depth}")
    public String getDistance(@PathParam("inflow") boolean inflow,
                              @PathParam("mi") int meanderIndex,
                              @PathParam("width") double width,
                              @PathParam("depth") float depth) throws JSONException {

        return new JSONStringer()
                .object()
                    .key("distance")
                    .value(new RiverDDModel().getDistance(inflow, meanderIndex, width, depth))
                .endObject()
                .toString();
    }
}
