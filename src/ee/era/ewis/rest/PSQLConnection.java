package ee.era.ewis.rest;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

public class PSQLConnection {
    final static String HOST = "ewis.pld.ttu.ee:5432";
    final static String DB = "ewis";
    final static String USER = "ewis";
    final static String PW = "A11ik45";

    public Connection con = null;
    public ResultSet rs = null;

    public PSQLConnection() {
        try{
            Class.forName("org.postgresql.Driver").newInstance();
            con = DriverManager.getConnection("jdbc:postgresql://" + HOST + "/" + DB, USER, PW); //andmebaas, kasutaja, parool
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    public ResultSet getResultSet(String query){
        try{
            Statement statement = con.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE);
            statement.executeQuery(query);
            rs = statement.getResultSet();
        }catch (Exception e){
            System.out.println(e);
        }
        return rs;
    }
}
