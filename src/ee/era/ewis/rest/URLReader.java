package ee.era.ewis.rest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;

/**
 * Read url into output string
 **/
public class URLReader {
    private URL url;
    public URLReader(URL url) {
        this.url = url;
    }

    public String getOutput() {
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(url.openStream()));
            StringBuilder builder = new StringBuilder();
            for (String line; (line = reader.readLine()) != null;) {
                builder.append(line).append("\n");
            }
            return builder.toString().toLowerCase();

        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public JSONObject getJSONOuptut() throws IOException, JSONException {
        JSONObject jsonObject = null;
        try {
            JSONTokener tokener = new JSONTokener(getOutput());
            JSONArray jsonArray = new JSONArray(tokener);
            jsonObject = jsonArray.getJSONObject(0);
        } catch (Exception e) {
            /*Empty json input*/
        }
        return jsonObject;
    }

}
