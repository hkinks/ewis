package ee.era.ewis.rest;

import ee.era.ewis.EwisLauncher;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import java.net.MalformedURLException;
import java.net.URL;


/**
 * Proxy service for querying map feature info from geoserver.
 */
@Path("getfeatureinfo")
@Produces("application/json")
public class GetFeatureInfoResource {

    @GET
    @Path("layername={layername}&bbox={bbox}&height={height}&width={width}&x={x}&y={y}")
    public String getMapObject( @PathParam("layername") String layername,
                                         @PathParam("bbox") String bbox,
                                         @PathParam("height") int height,
                                         @PathParam("width") int width,
                                         @PathParam("x") int x,
                                         @PathParam("y") int y) throws MalformedURLException {
        URL url = new URL("http://"+ EwisLauncher.HOST +"/geoserver/ewis/wms?service=WMS"+
                "&version=1.1.0" +
                "&request=GetFeatureInfo" +
                "&layers=" + layername +
                "&query_layers=" + layername +
                "&styles=&bbox=" + bbox +
                "&feature_count=1" +
                "&height=" + height +
                "&width=" + width +
                "&format=image%2fpng" +
                "&info_format=text%2fhtml" +
                "&srs=epsg%3a3301" +
                "&x=" + x +
                "&y=" + y);

        return new URLReader(url).getOutput();
    }

}
