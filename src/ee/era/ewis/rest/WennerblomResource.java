package ee.era.ewis.rest;

import ee.era.ewis.model.WennerblomModel;
import ee.era.ewis.model.testModels.Keila1;
import ee.era.ewis.model.testModels.Keila2;
import ee.era.ewis.model.testModels.Keila3;
import org.json.JSONException;
import org.json.JSONStringer;
import org.json.JSONWriter;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;

@Path("wennerblom")
@Produces("application/json")
public class WennerblomResource {
    private String inputFields[];

    public WennerblomResource() {
        inputFields = new String[]{"B2","B3","B4","B5","B7","B8","B9","B10","B11","B12","B13","B14","B15","B17","B18","B34","B35",
                "B36","B37","B38","B39","B40","B41","B42","B43","B44","B47","B48","B49","B50","B51","B52","B53","B54","B55",
                "B56","B57","B58","B59","B60","B61","B62","B63","B64","B65","B66","B69","B70","B71","B68","B72"};
    }

    @GET
    @Path("sample/{sample}")
    public String getSample(@PathParam("sample") String sample) throws JSONException, NoSuchMethodException {
        WennerblomModel wb = null;

        if(sample.equals("keila1"))wb = new Keila1();
        if(sample.equals("keila2"))wb = new Keila2();
        if(sample.equals("keila3"))wb = new Keila3();

        if ( wb != null ){
            JSONStringer stringer = new JSONStringer();
            JSONWriter array = stringer.array();

            for (String inputField : inputFields) {
                try {
                    array.object()
                            .key(inputField)
                            .value(wb.getClass()
                                    .getMethod("get" + inputField)
                                    .invoke(wb))
                            .endObject();
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                } catch (InvocationTargetException e) {
                    e.printStackTrace();
                }
            }
            stringer.endArray();

            return stringer.toString();

        }
        return "Sample not found";
    }

    @GET
    @Path("calc/{data}")
    public String calculate(@PathParam("data") String data) throws JSONException {
        WennerblomModel model = new WennerblomModel();
        ArrayList<String> dataArray =  new ArrayList<String>(Arrays.asList(data.split("&")));
        Iterator<String> iterator = dataArray.iterator();


        model.setB2(iterator.next());
        model.setB3(Integer.valueOf(iterator.next()));
        Class[] argtypes = {};
        for (String inputField : inputFields) {
            if(!inputField.equals("B2") && !inputField.equals("B3")){
                try {
                    model.getClass().getMethod("set"+inputField, double.class).invoke(model, Double.valueOf(iterator.next()));
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                } catch (InvocationTargetException e) {
                    e.printStackTrace();
                } catch (NoSuchMethodException e) {
                    e.printStackTrace();
                }
            }
        }

        JSONStringer stringer = (JSONStringer) new JSONStringer()
                .object()
                    .key("N9")
                    .value(roundDec(model.getN9()))
                    .key("N9p")
                    .value(roundDec(model.getN9() * 100 / model.getN21()))
                    .key("P9")
                    .value(roundDec(model.getP9()))
                    .key("P9p")
                    .value(roundDec(model.getP9() * 100 / model.getP21()))
                    .key("N10")
                    .value(roundDec(model.getN10()))
                    .key("N10p")
                    .value(roundDec(model.getN10() * 100 / model.getN21()))
                    .key("P10")
                    .value(roundDec(model.getP10()))
                    .key("P10p")
                    .value(roundDec(model.getP10() * 100 / model.getP21()))
                    .key("N11")
                    .value(roundDec(model.getN11()))
                    .key("N11p")
                    .value(roundDec(model.getN11() * 100 / model.getN21()))
                    .key("P11")
                    .value(roundDec(model.getP11()))
                    .key("P11p")
                    .value(roundDec(model.getP11() * 100 / model.getP21()))
                    .key("N12")
                    .value(roundDec(model.getN12()))
                    .key("N12p")
                    .value(roundDec(model.getN12() * 100 / model.getN21()))
                    .key("P12")
                    .value(roundDec(model.getP12()))
                    .key("P12p")
                    .value(roundDec(model.getP12() * 100 / model.getP21()))
                    .key("N13")
                    .value(roundDec(model.getN13()))
                    .key("N13p")
                    .value(roundDec(model.getN13() * 100 / model.getN21()))
                    .key("P13")
                    .value(roundDec(model.getP13()))
                    .key("P13p")
                    .value(roundDec(model.getP13() * 100 / model.getP21()))
                    .key("N14")
                    .value(roundDec(model.getN14()))
                    .key("N14p")
                    .value(roundDec(model.getN14() * 100 / model.getN21()))
                    .key("P14")
                    .value(roundDec(model.getP14()))
                    .key("P14p")
                    .value(roundDec(model.getP14() * 100 / model.getP21()))
                    .key("N15")
                    .value(roundDec(model.getN15()))
                    .key("N15p")
                    .value(roundDec(model.getN15() * 100 / model.getN21()))
                    .key("P15")
                    .value(roundDec(model.getP15()))
                    .key("P15p")
                    .value(roundDec(model.getP15() * 100 / model.getP21()))
                    .key("N16")
                    .value(roundDec(model.getN16()))
                    .key("N16p")
                    .value(roundDec(model.getN16() * 100 / model.getN21()))
                    .key("P16")
                    .value(roundDec(model.getP16()))
                    .key("P16p")
                    .value(roundDec(model.getP16() * 100 / model.getP21()))
                    .key("N17")
                    .value(roundDec(model.getN17()))
                    .key("N17p")
                    .value(roundDec(model.getN17() * 100 / model.getN21()))
                    .key("P17")
                    .value(roundDec(model.getP17()))
                    .key("P17p")
                    .value(roundDec(model.getP17() * 100 / model.getP21()))
                    .key("N18")
                    .value(roundDec(model.getN18()))
                    .key("N18p")
                    .value(roundDec(model.getN18() * 100 / model.getN21()))
                    .key("P18")
                    .value(roundDec(model.getP18()))
                    .key("P18p")
                    .value(roundDec(model.getP18() * 100 / model.getP21()))
                    .key("N19")
                    .value(roundDec(model.getN19()))
                    .key("N19p")
                    .value(roundDec(model.getN19() * 100 / model.getN21()))
                    .key("P19")
                    .value(roundDec(model.getP19()))
                    .key("P19p")
                    .value(roundDec(model.getP19() * 100 / model.getP21()))
                    .key("N20")
                    .value(roundDec(model.getN20()))
                    .key("N20p")
                    .value(roundDec(model.getN20() * 100 / model.getN21()))
                    .key("P20")
                    .value(roundDec(model.getP20()))
                    .key("P20p")
                    .value(roundDec(model.getP20() * 100 / model.getP21()))
                    .key("N21")
                    .value(roundDec(model.getN21()))
                    .key("N21p")
                    .value(100)
                    .key("P21")
                    .value(roundDec(model.getP21()))
                    .key("P21p")
                    .value(100)

                    .key("N30")
                    .value(roundDec(model.getN30()))
                    .key("N30p")
                    .value(roundDec(model.getN30() * 100 / model.getN36()))
                    .key("P30")
                    .value(roundDec(model.getP30()))
                    .key("P30p")
                    .value(roundDec(model.getP30() * 100 / model.getP36()))
                    .key("N31")
                    .value(roundDec(model.getN31()))
                    .key("N31p")
                    .value(roundDec(model.getN31() * 100 / model.getN36()))
                    .key("P31")
                    .value(roundDec(model.getP31()))
                    .key("P31p")
                    .value(roundDec(model.getP31() * 100 / model.getP36()))
                    .key("N32")
                    .value(roundDec(model.getN32()))
                    .key("N32p")
                    .value(roundDec(model.getN32() * 100 / model.getN36()))
                    .key("P32")
                    .value(roundDec(model.getP32()))
                    .key("P32p")
                    .value(roundDec(model.getP32() * 100 / model.getP36()))
                    .key("N33")
                    .value(roundDec(model.getN33()))
                    .key("N33p")
                    .value(roundDec(model.getN33() * 100 / model.getN36()))
                    .key("P33")
                    .value(roundDec(model.getP33()))
                    .key("P33p")
                    .value(roundDec(model.getP33() * 100 / model.getP36()))
                    .key("N34")
                    .value(roundDec(model.getN34()))
                    .key("N34p")
                    .value(roundDec(model.getN34() * 100 / model.getN36()))
                    .key("P34")
                    .value(roundDec(model.getP34()))
                    .key("P34p")
                    .value(roundDec(model.getP34() * 100 / model.getP36()))
                    .key("N36")
                    .value(roundDec(model.getN36()))
                    .key("N36p")
                    .value(roundDec(model.getN36() * 100 / model.getN58()))
                    .key("P36")
                    .value(roundDec(model.getP36()))
                    .key("P36p")
                    .value(roundDec(model.getP36() * 100 / model.getP58()))

                .key("N40")
                    .value(roundDec(model.getN40()))
                    .key("N41")
                    .value(roundDec(model.getN41()))
                    .key("N42")
                    .value(roundDec(model.getN42()))
                    .key("N43")
                    .value(roundDec(model.getN43()))
                    .key("N44")
                    .value(roundDec(model.getN44()))
                    .key("N45")
                    .value(roundDec(model.getN45()))
                    .key("N46")
                    .value(roundDec(model.getN46()))
                    .key("N47")
                    .value(roundDec(model.getN47()))
                    .key("N49")
                    .value(roundDec(model.getN49()))
                    .key("N50")
                    .value(roundDec(model.getN50()))
                    .key("N51")
                    .value(roundDec(model.getN51()))
                    .key("N52")
                    .value(roundDec(model.getN52()))
                    .key("N53")
                    .value(roundDec(model.getN53()))

                .endObject();
        return stringer.toString();
    }

    private double roundDec(double a) {
        return Math.round(a*100.0)/100.0;
    }


}