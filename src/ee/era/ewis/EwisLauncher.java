package ee.era.ewis;

import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.nio.SelectChannelConnector;
import org.eclipse.jetty.webapp.WebAppContext;

public class EwisLauncher {
    public static final String HOST = "ewis.pld.ttu.ee";

    public static void main(String[] args) throws Exception {
        int DEFAULT_PORT = 8080;

        Server server = new Server();
        int port = args.length > 0 ? Integer.valueOf(args[0]) : DEFAULT_PORT;

        SelectChannelConnector connector = new SelectChannelConnector();
        connector.setPort(port);
        server.addConnector(connector);

        WebAppContext context = new WebAppContext("webapp", "/");

        if (System.getProperty("os.name").toLowerCase().contains("windows")) {
            // fix for Windows, so Jetty doesn't lock files
            context.getInitParams().put("org.eclipse.jetty.servlet.Default.useFileMappedBuffer", "false");
        }

        server.setHandler(context);
        server.start();
    }
}
