package ee.era.ewis.model.testModels;

import ee.era.ewis.model.WennerblomModel;

public class Keila3 extends WennerblomModel {
	
	public Keila3() {
		setB2("Keila_3, 1096100_3");
		setB4(3.67);  //pindala kokku
		//setB5();  //vooluhulk
		setB7(2.27);  //mets
		setB13(0);    //soo
		setB15(0.66);	  //p�ld
		setB34(0);   //loom�
		setB35(0);   //veised
		
		/*
		 * Kits, Lammas = 1/10 L�
		 * Siga = 1/2 L�
		 * Veis = 1L�
		 */
	}
	
}
