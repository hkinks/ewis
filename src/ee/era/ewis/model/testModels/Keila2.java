package ee.era.ewis.model.testModels;

import ee.era.ewis.model.WennerblomModel;

public class Keila2 extends WennerblomModel {
	
	public Keila2() {
		setB2("Keila_2, 1096100_2");
		setB4(211.89);  //pindala kokku
		setB5(4.35);  //vooluhulk
		setB7(114.65);  //mets
		setB13(5.32);    //soo
		setB15(76.47);	  //p�ld
		setB34(39);   //loom�
		setB35(2838);   //veised
		
		/*
		 * Kits, Lammas = 1/10 L�
		 * Siga = 1/2 L�
		 * Veis = 1L�
		 */
	}
	
}
