package ee.era.ewis.model;

public class River {
    private Long id;
    private String name;
    private float length;
    private float depth;
    private Location startP;
    private Location endP;
    private float area;

    @Override
    public String toString() {
        return "River{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", width=" + getWidth() +
                ", length=" + length +
                ", distance=" + getDistance() +
                ", depth=" + depth +
                ", meanderIndex=" + getMeanderIndex() +
                ", startP=" + startP +
                ", endP=" + endP +
                '}';
    }

    public float getDistance() {
        float latDif = endP.getLatitude()-startP.getLatitude();
        float lonDif = endP.getLongitude()-startP.getLongitude();
        return (float) Math.sqrt(latDif*latDif + lonDif*lonDif);
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public double getWidth() {
        return Math.round((area / length) * 100.0) / 100.0;
    }

    public float getLength() {
        return length;
    }

    public float getDepth() {
        return depth;
    }

    public int getMeanderIndex() {
        int ratio =(int) (getLength()/getDistance());

        int index;
        if (ratio>500) index = 9;
        else {
            index = (int) (ratio * 0.02);
        }

        return index;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setLength(Float length) {
        this.length = length;
    }

    public void setDepth(Float depth) {
        this.depth = depth;
    }

    public void setBeginningPoint(float x, float y) {
        this.startP = new Location(x,y);
    }

    public void setEndingPoint(float x, float y) {
        this.endP = new Location(x,y);
    }

    public void setArea(float area) {
        this.area = area;
    }
}
