package ee.era.ewis.model;


/**
 * @author Hannes Kinks
 * Arvutusmudel leidmaks j6kke sisselastud koormusallika t2ielikult lebisegunenud
 * ristl6ike kagust sisselaske punktist.
 *
 * Kaks meetodit: v6imalus j6e objekt ette anda v6i k6ik vajalikud parameetrid
 */
public class RiverDDModel{

    public double calculateOnRiver(River river, boolean centerInFlow){
        return getDistance(centerInFlow, river.getMeanderIndex(), river.getWidth(), river.getDepth());
    }

    public double getDistance(boolean centerInFlow, int meanderIndex, double width, float depth) {
        float dy = (float) ((meanderIndex +1)*(Math.pow(10,(-3.547))*(Math.pow(width / depth,1.378))* depth));

        /*Koefitsendi m22ramine olenevalt, kas sisselase asub j6e keskel v6i 22res*/
        float coef = centerInFlow ? 0.4f : 0.1f;
        double distance = coef * Math.pow(width, 2)/dy;

        return Math.round(distance*100.0)/100.0;
    }
}
