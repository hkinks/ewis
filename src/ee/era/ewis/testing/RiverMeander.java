package ee.era.ewis.testing;

import ee.era.ewis.model.River;
import ee.era.ewis.model.RiverDDModel;
import ee.era.ewis.rest.PSQLConnection;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/*Class for testing riverdd model and river meander index*/
public class RiverMeander {
    public static void main(String[] args) throws SQLException, IOException {
        PSQLConnection connection = new PSQLConnection();
        ResultSet resultSet = connection.getResultSet("SELECT pikkus_km,pindala_km2,algus_x,algus_y,lopp_x,lopp_y FROM joe_andmed");
//        ResultSet resultSet = connection.getResultSet("SELECT pikkus_km,algus_x,algus_y,lopp_x,lopp_y FROM joe_andmed WHERE id_jogi='1152500'");
        List<River> riverList = new ArrayList<River>();

        while(resultSet.next()){
            River river = new River();
            river.setLength(resultSet.getFloat(1));
            String areaString = resultSet.getString(2);
            if(!areaString.isEmpty())river.setArea(Float.valueOf(resultSet.getString(2)));
            river.setDepth(1.0f);
            river.setBeginningPoint(resultSet.getFloat(3), resultSet.getFloat(4));
            river.setEndingPoint(resultSet.getFloat(5), resultSet.getFloat(6));

            riverList.add(river);
        }

        for (River river : riverList) {
            double ddistance = new RiverDDModel().calculateOnRiver(river, true);
            System.out.println( river.toString() );
            System.out.format( "DDISTANCE: %f \n", ddistance);
        }

    }
}
